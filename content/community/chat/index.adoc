+++
title = "Discord & IRC"
aliases = [ "/community/irc/" ]
[menu.main]
    parent = "Community"
    name   = "Discord & IRC"
    weight = 15
+++
:linkattrs:

== Discord
Discord is a group chat platform originally intended for gamers but now popular for use for anything! 
Bringing modern chat features to the table, it is easy to use and cross platform (desktops and mobile)

image::discord.png[link="https://discord.gg/FANuKv8sZn"]


== IRC

Internet Relay Chat (IRC) is a group chat system that is popular in many open source communities.
There is a very active community channel called `#kicad` on the link:https://libera.chat/[Libera.Chat]
IRC network where KiCad users can get help, advice, or just generally chat.

You can access it from a web client by clicking the following link, choosing a nickname,
and pressing Start.

link:https://web.libera.chat/#kicad[Connect to #\kicad@irc.libera.chat]
