+++
title = "System Requirements"
summary = "Detailed system requirements for the various platforms supported by KiCad"
aliases = [ "/post/system-requirements/" ]
[menu.main]
    parent = "Help"
    name   = "System Requirements"
    weight = 50
+++
:toc: macro
:toc-title:

toc::[]

== Common System Requirements (hardware specs)

* Supported Processor Architectures
** Intel (or compatible) 32 and 64 bit.
** PowerPC 64 bit.
** ARM 64 bit.
** MIPS 64 bit.

* 1GB RAM (>2G RAM recommended);

* Up to 10GB available hard disk space;

* Screen resolution of 1280x1024 works but 1920x1080 or higher is recommended.
  At lower resolutions toolbar buttons may be hidden but corresponding
  commands should be reachable via menu.

* Graphics card with OpenGL 2.1 or greater that supports hardware shaders. Check
  the OpenGL specs of the card:
** Intel: cards that are reported working start from model HD2000 and higher.
** NVIDIA: from NVIDIA support page: http://www.nvidia.com/object/nv_ogl2_support.html
   NVIDIA support for OpenGL 2.0 begins with the Release 75 series of drivers.
   GeForce FX (NV3x), GeForce 6 Series (NV4x), NV3xGL-based Quadro FX and NV4xGL-based
   Quadro FX GPUs, and all future NVIDIA GPUs support OpenGL 2.0.
** AMD/ATI: should work any modern Radeon GPU

== Common System Requirements (software specs)

See: link:/help/known-system-related-issues/[Known system related issues] for software specific issues.

== Specific System Requirements

KiCad supports major operating systems that remain supported by their developers.  After the
operating systems' respective organizations stop releasing updates for the system, KiCad will
not be specifically tested with the unsupported system.  Unsupported operating systems may
continue to work with KiCad beyond this time but bugs must be reproduced on a supported operating
system before they will be addressed by KiCad.

=== Windows

The software prerequisites for installing KiCad on a Windows system are as follows:

[%header,width="75%",cols="^2,4",role="table table-striped table-condensed"]
|===
|Operating System       | Support Status
|Windows 7              | Unsupported
|Windows 8              | Unsupported, will most likely still function
|Windows 8.1            | Unsupported, will most likely still function
|Windows Server 2012    | Unsupported, will most likely still function
|Windows Server 2016    | 10-Oct-2027
|Windows Server 2019    | 09-Jan-2029
|Windows Server 2022    | 14-Oct-2031
|Windows 10             | 14-Oct-2025
|Windows 11             | TBD
|===

[%hardbreaks]
=== Apple - macOS

KiCad provides official support for macOS versions that are currently supported by Apple.  Apple
does not provide formal support timelines, but typically provides support and security updates for
the three most recent major macOS versions.  Based on this, the KiCad project considers a macOS
version to be end-of-life when it is not one of the three most recent major releases.

While the KiCad project does not support or test older macOS versions, it is possible that KiCad
can be installed and run on older systems.  Any issues or bugs that occur on an unsupported version
of macOS must be reproduced on an officially-supported version before they will be addressed by the
KiCad team.

A community-provided port of KiCad is available on https://ports.macports.org/port/kicad/[MacPorts],
which may provide an option for users on unsupported versions of macOS to install a more recent
version.

[%header,width="50%",role="table table-striped table-condensed"]
|===
|Operating System   | End of Support
|macOS 11 and older | Unsupported
|macOS 12.x         | 1-Nov-2024 (approximately)
|macOS 13.x         | 1-Nov-2025 (approximately)
|macOS 14.x         | 1-Nov-2026 (approximately)
|===

[%hardbreaks]
=== GNU/Linux

The following Linux distributions have been tested and are known to be working with
the stable release version of KiCad.
Issues or bugs that occur on an unsupported platform must be reproduced on an officially
supported distribution and window manager before they will be addressed by KiCad.

*Note:* Development versions of the next KiCad release may not function
on some of the older distributions listed below.


==== Ubuntu

[%header,width="75%",cols="^2,4",role="table table-striped table-condensed"]
|===
|Operating System       |End of Support
|Ubuntu 20.04 (LTS)     |Unsupported, will most likely still function
|Ubuntu 22.04 (LTS)     |May 2025
|Ubuntu 23.10           |Nov 2024
|===

Long Term Support (LTS) releases of Ubuntu will be supported for 1 year after the next
LTS version is released (for a total of 3 years of support).

Non-LTS releases of Ubuntu will be supported while the version
is under https://wiki.ubuntu.com/Releases[standard support] from Ubuntu.


==== Fedora

[%header,width="75%",cols="^2,4",role="table table-striped table-condensed"]
|===
|Operating System       |End of Support
|Fedora 39              |November 2024
|Fedora 40              |May 2025
|Fedora 41              |November 2025
|===

Fedora releases will be supported for as long as they are supported by the Fedora Project
(usually for 1 year after release).


==== Debian

[%header,width="75%",cols="^2,4",role="table table-striped table-condensed"]
|===
|Operating System       |End of Support
|Debian 9 (Stretch)     |Unsupported, will most likely still function
|Debian 10 (Buster)     |Unsupported, will most likely still function
|Debian 11 (Bullseye)   |approx 2024
|Debian 12 (Bookworm)   |approx 2025
|===

Debian releases will be supported for as long as they are
https://wiki.debian.org/DebianReleases#Production_Releases[supported] by Debian.

[%hardbreaks]
==== Additional Linux Considerations
Linux allows users to select their preferred window manager.  There are many esoteric window
managers available for Linux and some may have unexpected behavior.  KiCad officially supports
the following window managers:

* Metacity (used by GNOME 2 and GNOME flashback)
* Mutter (GNOME 3)
* KWin (KDE)
* Xfwm (used by XFCE)

Other window managers may or may not work; bugs seen using them must be reproduced in a supported
environment before they will be investigated by the KiCad team.  Some window managers are known to
have bugs or issues that affect KiCad, including:

* Muffin (used by Cinnamon)

==== Graphical Windowing Backend
Regardless of the window manager, KiCad officially only supports the X11 backend.  Users who
choose to use Wayland will have to run KiCad in the compatibility layer
link:https://wayland.freedesktop.org/xserver.html[XWayland].

Issues or bugs encountered while using XWayland must be reproduced under X11 before they
will be addressed by KiCad.  Bugs that cannot be reproduced on X11 should be reported to
the link:https://gitlab.freedesktop.org/wayland/wayland/issues[Wayland bug tracker].


=== Other OSes

Other systems (notably Unix *BSD) may be fully functional but are not officially supported.
