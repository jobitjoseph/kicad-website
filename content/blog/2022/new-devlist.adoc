+++
title = "New Developer Mailing List"
date = "2022-07-07"
draft = false
"blog/categories" = [
    "News"
]
+++

After 13 years, it is time to retire the Launchpad developer mailing list.
The new developer mailing list will be hosted on the KiCad Google Workspace
instance at devlist@kicad.org.  You can subscribe to the new list by sending
an e-mail to devlist+subscribe@kicad.org

<!--more-->

== FAQs ==
=== Why are we making this change? ===
Over the past two years, mail delivery via the Launchpad mailing list has become
increasingly spotty.  Sometimes mail does not reach the launchpad archive (and 
therefore does not get delivered) and sometimes mail reaches the archive but does
not get delivered to the recipients.

In neither case does the KiCad team have visibility on the rejected messages or
any ability to recitify the problem.  This leads to many messages to the list
going unanswered until someone directly asks a KiCad team member about
the mailing list.

=== What options did we consider? ===
When migrating our repository from Launchpad, we considered a number of different 
list servers including self-hosting mailman, groups.io, Framalistes and GitHub.  While
each of these would perform some parts of what we wanted, we needed to minimize our
system administration time (so that we can do more KiCad coding) while not
excessively increasing our monthly costs.

From this point of view, staging the mailing list on our Google Workspace instance
made the most sense.  We are able to manage the list in detail and get 
administrative feedback when messages are rejected or delayed.  We also get to 
ensure that the list and its data are fully owned by the KiCad team.  This will 
allow us to migrate the list more easily in the future, should the Google
instance no longer be tenable.

=== I have issues with Google... ===
We understand that some of our users are justifiably suspicious of sending mail
though a Google-hosted service.  While we cannot guarantee any behaviour on the part
of Google, we can say that this service is part of the corporate Google Workspace package
that currently promises not to scan any data contained within it (https://workspace.google.com/learn-more/security/security-whitepaper/page-6.html).

=== How do I access the list? ===
You can send an e-mail to devlist+subscribe@kicad.org. You do not need a Google-based
e-mail address to subscribe to the mailing list.  You can view the archives at https://groups.google.com/a/kicad.org/g/devlist without a Google account as well.