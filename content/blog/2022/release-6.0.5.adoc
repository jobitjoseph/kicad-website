+++
title = "KiCad 6.0.5 Release"
date = "2022-05-04"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.5 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 6.0.4 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/15[KiCad 6.0.5
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 6.0.5 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fixed the unique GUIDs being generated for objects not being seeded at launch which caused them
to repeat each restart of KiCad
- Fix significant resource leak over long term usage sessions that notably
would result in total loss of window function on Windows (https://gitlab.com/kicad/code/kicad/-/issues/10994[#10994])
- The Open Recent and Clear files menu titles will now update after a language change (https://gitlab.com/kicad/code/kicad/-/commit/2de1bd7a4a2cf5eff28196e4c44fff3eae27c903[2de1bd7a])
- Fixed syntax highlighting of matching parenthesis in text input boxes (https://gitlab.com/kicad/code/kicad/-/issues/11349[#11349])

=== Schematic Editor
- Importing CADSTAR schematics where components with alternate symbols enabled will now import correctly
(https://gitlab.com/kicad/code/kicad/-/issues/11378[#11738])
- Pasted and moved components that have offgrid pins at start will now snap to the grid (https://gitlab.com/kicad/code/kicad/-/issues/11360[#11360])
- Update Symbols from Library and ERC will now ignore name differences in place symbols (https://gitlab.com/kicad/code/kicad/-/issues/10783[#10783])
- Junctions will now be added when a component is rotated in place and they are required (https://gitlab.com/kicad/code/kicad/-/issues/11242[#11242])
- Improved schematic sheet import messages and logic (https://gitlab.com/kicad/code/kicad/-/issues/11075[#11075])
- Plotting/printing is no longer arbitrarily clamped to 60 inches (https://gitlab.com/kicad/code/kicad/-/issues/11196[#1196])
- Fix a condition where subsheets may have value and footprint fields erased (https://gitlab.com/kicad/code/kicad/-/issues/11194[#11194])
- The _Edit Text & Graphics Properties_ dialog will now update Sheet Pins (https://gitlab.com/kicad/code/kicad/-/issues/11297[#11297])
- Fix hierarchical sheet instance blocks not aligning to grid well (https://gitlab.com/kicad/code/kicad/-/issues/11201[#11201])
- Fixed crash when selecting a item and then triggering a external design file import (https://gitlab.com/kicad/code/kicad/-/issues/10145[#10145])
- Fixed subscript/superscript escaping logic that failed in certain cases and resulted in bad net names (https://gitlab.com/kicad/code/kicad/-/issues/11093[#11093])
- Fixed a case where the place tool location may warp between dialog invocation (https://gitlab.com/kicad/code/kicad/-/issues/11057[#11057])
- Add potentially large performance improvement on larger schematic symbols when generating connectivity data (https://gitlab.com/kicad/code/kicad/-/issues/10974[#10974])
- Bulk editing fields on a component will no longer be lost in certain circumstances (https://gitlab.com/kicad/code/kicad/-/issues/11439[#11439])


=== Symbol Editor
- A crash when editing symbol properties was fixed (https://gitlab.com/kicad/code/kicad/-/commit/851f893e8e4c674cc7435b05ad20a249c90eb6de[851f89])
- Fixed crash when sorting the pin table and a certain pin names were used (https://gitlab.com/kicad/code/kicad/-/commit/cf5f36aac5e37cfba2149b0c916c8f408be6b473[cf5f36aac])
- Fixed a case where editing a symbol in the context of the schematic could accidentally get saved to library without asking (https://gitlab.com/kicad/code/kicad/-/issues/10767[#10767])
- The DeMorgan checkbox will now work when creating a symbol (https://gitlab.com/kicad/code/kicad/-/issues/11199[#11199])
- Pin electrical type names will now translate correctly after a language change (https://gitlab.com/kicad/code/kicad/-/issues/11324[11324])
- Removed a false warning message about being in "synchronized pin mode" (https://gitlab.com/kicad/code/kicad/-/issues/11331[#11331])
- Improved the modified symbol status check to avoid false indication (https://gitlab.com/kicad/code/kicad/-/issues/10791[#10791])
- Symbols may now be pasted into empty libraries (https://gitlab.com/kicad/code/kicad/-/issues/11053[#11053])
- The De Morgan checkbox in symbol properties will no longer uncheck itself (https://gitlab.com/kicad/code/kicad/-/issues/11200[#11200])

=== PCB Editor
- A crash when selecting DRC markers was fixed (https://gitlab.com/kicad/code/kicad/-/commit/110663d214929f7cb072473d500c1c551737ac7f[110663d21])
- DRC rules will now apply to arcs correctly (https://gitlab.com/kicad/code/kicad/-/issues/11384[#11384])
- STEP exports will include arcs in polygon paths correctly (https://gitlab.com/kicad/code/kicad/-/issues/11389[#11389])
- Various fixes were made to import of EAGLE PCB dimension objects (https://gitlab.com/kicad/code/kicad/-/issues/10763[#10763])
- Fix a few cases of zone fill artifacts (https://gitlab.com/kicad/code/kicad/-/issues/6907[#6907])
- Improved error reporting for `diff_pair_uncoupled` design rule constraints (https://gitlab.com/kicad/code/kicad/-/issues/10087[#10087])
- Running DRC with zone fill disabled will no longer wrongly mark the PCB window as having a change to the board file (https://gitlab.com/kicad/code/kicad/-/issues/11344[#11344])
- The selection rectangle when zoomed far out will no longer inverse when hitting the work area limits (https://gitlab.com/kicad/code/kicad/-/issues/8846[#11344])
- Improve VRML export performance https://gitlab.com/kicad/code/kicad/-/commit/5a68077f60cc0b0cf9fec9a694b250419c880aef[5a68077]
- Fixed the start point of tracks when the grid was coarse and the pad is far off grid (https://gitlab.com/kicad/code/kicad/-/issues/11239[#11239])
- The "Allow DRC Violations" router setting will no longer get lost during route mode changes (https://gitlab.com/kicad/code/kicad/-/issues/11177[#11177])
- An attempt is now made to handle running out of memory on import of oversized DXF imports to avoid a crash (https://gitlab.com/kicad/code/kicad/-/issues/11308[#11308])
- Improved performance for the _Cleanup Tracks & Vias_ tool for large boards (https://gitlab.com/kicad/code/kicad/-/commit/f69ebe55adb06aec5a95fefb4ec10efe139a2524[f69ebe55])
- Fixed PAD.AddPrimitive python invocation not accepting polygons (https://gitlab.com/kicad/code/kicad/-/issues/11012[#11012])
- Improved variable expansion in title blocks (https://gitlab.com/kicad/code/kicad/-/issues/11168[#11168])
- Improved Specctra import to remove memory leak, handle locked tracks, and more (https://gitlab.com/kicad/code/kicad/-/commit/1b108afd0b2d0a7c424393f2078637e3e23cf2fd[1b108af])
- Fixed crash triggering a zone fill after using the remove unused pads option (https://gitlab.com/kicad/code/kicad/-/issues/11114[#11114])
- Fixed case where DRC may use the wrong constraint value (https://gitlab.com/kicad/code/kicad/-/issues/11197[#11197])
- Fixed crash when using python to generate a gerber job file (https://gitlab.com/kicad/code/kicad/-/issues/11227[#11227])
- The _Annototate All_ option in the _Rennotate_ dialog will no longer be checked by default if you opened the dialog with an empty selection (https://gitlab.com/kicad/code/kicad/-/issues/11181[#11181])
- Fixed importing certain pads from CircuitStudio (https://gitlab.com/kicad/code/kicad/-/issues/10493[#10493])
- Blind and micro vias will be enabled correctly on import from Altium with their presence (https://gitlab.com/kicad/code/kicad/-/issues/10044[#10044])

=== Footprint Editor
- Footprint embedded zones now have snapping points allowing for use of measure tools (https://gitlab.com/kicad/code/kicad/-/issues/10461[#10461])

=== 3D Viewer
- Plated pads with certain pad stack definitions will now render correctly as plated (https://gitlab.com/kicad/code/kicad/-/issues/10207[#10207])
- Prevent missing render data when using certain AMD video cards (https://gitlab.com/kicad/code/kicad/-/issues/10741[#10741])

=== Windows Platform
- Fixed PYTHON_HOME environment variables on the System environment causing KiCad not to start (https://gitlab.com/kicad/code/kicad/-/issues/11328[#11328])


=== macOS Platform
- Added a hack to try and resize grids to the current monitor DPI if a window is moved (https://gitlab.com/kicad/code/kicad/-/commit/d9fe48171b1c0306ce9f427a76c4eeb270d42f44[d9fe48])
