+++
title = "KiCad 6.0.1 Release"
date = "2022-01-15"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the latest series 6 stable
release.  The 6.0.1 stable version contains critical bug fixes and
other minor improvements since the previous release.

<!--more-->

A list of all of the fixed bugs since the 6.0.0 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/10[KiCad 6.0.1
milestone page].  This release contains several critical bug fixes so
please consider upgrading as soon as possible.

Version 6.0.1 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/6.0/[6.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.
