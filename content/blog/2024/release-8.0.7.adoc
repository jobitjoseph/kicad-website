+++
title = "KiCad 8.0.7 Release"
date = "2024-12-03"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 8.0.7 bug fix release.
The 8.0.7 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 8.0.6 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/41[KiCad 8.0.7
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 8.0.7 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/8.0/[8.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix URI environment variable expansion issue. https://gitlab.com/kicad/code/kicad/-/issues/18918[#18918]
- Fix display of broken fonts. https://gitlab.com/kicad/code/kicad/-/issues/19010[#19010]
- Clear unknown keys from environment variable maps on save. https://gitlab.com/kicad/code/kicad/-/issues/18734[#18734]
- Exclude letter hotkeys from shift fallback. https://gitlab.com/kicad/code/kicad/-/issues/19093[#19093]
- Fix missing text and graphics in PDF plots. https://gitlab.com/kicad/code/kicad/-/issues/16456[#16456]
- Handle DXF files with out of bounds values. https://gitlab.com/kicad/code/kicad/-/issues/18523[#18523]
- Fix invalid Bezier curve DXF import. https://gitlab.com/kicad/code/kicad/-/issues/11153[#11153]
- Fix incorrectly rendered font depending on rotation. https://gitlab.com/kicad/code/kicad/-/issues/18377[#18377]
- Make color picker dialog usable on hidpi monitors. https://gitlab.com/kicad/code/kicad/-/issues/17860[#17860]
- Scale text size to inside scaled blocks on DXF import. https://gitlab.com/kicad/code/kicad/-/issues/18525[#18525]
- Fix copy-paste issue for grids with checkboxes. https://gitlab.com/kicad/code/kicad/-/issues/18985[#18985]
- Update font when needed on italic/bold change. https://gitlab.com/kicad/code/kicad/-/issues/18592[#18592]
- Prevent altering text thickness when switching bold option on and off. https://gitlab.com/kicad/code/kicad/-/issues/18975[#18975]


=== Schematic Editor
- Make sheet pin unconnected indicator (X) follow when dragging sheet. https://gitlab.com/kicad/code/kicad/-/issues/18854[#18854]
- Fix import compatibility with LCEDA/EasyEDA v2.2.32. https://gitlab.com/kicad/code/kicad/-/issues/18994[#18994]
- Use symbol/netport name from device entry if empty when importing EasyEDA/LCEDA schematic. https://gitlab.com/kicad/code/kicad/-/issues/19021[#19021]
- Do not import empty visible net labels from EasyEDA/LCEDA Pro schematics. https://gitlab.com/kicad/code/kicad/-/issues/19034[#19034]
- Import Bezier curves in symbols from EasyEDA/LCEDA schematics. https://gitlab.com/kicad/code/kicad/-/issues/19034.[#19034.]
- Test for valid sheet name in sheet dialog properties. https://gitlab.com/kicad/code/kicad/-/issues/18981[#18981]
- Fix missing actions strings and hotkey conflict. https://gitlab.com/kicad/code/kicad/-/issues/17694[#17694]
- Respect background color fill when plotting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/18919[#18919]
- Fix copy and paste issue in bus alias definition. https://gitlab.com/kicad/code/kicad/-/issues/18558[#18558]
- Do not allow repeat action to cause sheet recursion. https://gitlab.com/kicad/code/kicad/-/issues/18199[#18199]
- Fix crash when importing Eagle schematic when board file doesn't exist in non-stand alone mode. https://gitlab.com/kicad/code/kicad/-/issues/18241[#18241]
- Warn user when template field names contain trailing/leading white space.https://gitlab.com/kicad/code/kicad/-/issues/18601[#18601]
- Fix an issue in sheet cross-references of global labels. https://gitlab.com/kicad/code/kicad/-/issues/18534[#18534]
- Add sheet changes to net navigation history. https://gitlab.com/kicad/code/kicad/-/issues/18616[#18616]


=== Symbol Editor.
- https://gitlab.com/kicad/code/kicad/-/commit/51a3859d7e07c9ea1cef2be0886c12a7ca268113[Preserve pin numbering when duplicating].


=== Spice Simulator
- Fix missing current scale in AC simulation. https://gitlab.com/kicad/code/kicad/-/issues/18313[#18313]
- Fix crash when deselecting the probe tool. https://gitlab.com/kicad/code/kicad/-/issues/18547[#18547]


=== Board Editor
- https://gitlab.com/kicad/code/kicad/-/commit/0c8172880f8936dcf56998a013c7ea07fb79a837[Prevent solder mask bridge DRC error with mask pad].
- Report implicit design rule minimums as being from the board setup. https://gitlab.com/kicad/code/kicad/-/issues/18642[#18642]
- Fix importing custom design rules. https://gitlab.com/kicad/code/kicad/-/issues/18580[#18580]
- Don't ask about locking twice. https://gitlab.com/kicad/code/kicad/-/issues/18679[#18679]
- https://gitlab.com/kicad/code/kicad/-/commit/973d2d6c0329eaceff3c10bad4678def797898cc[Make orphaned nets obey the minimum board clearance].
- Make neck in a zone fill the DRC epsilon smaller on each side. https://gitlab.com/kicad/code/kicad/-/issues/18921[#18921]
- Make sure paste action can't be performed through modal dialog. https://gitlab.com/kicad/code/kicad/-/issues/18912[#18912]
- Make polyline properties dialog honor the escape key. https://gitlab.com/kicad/code/kicad/-/issues/18850[#18850]
- Fix focus issue in parent window after hiding the search panel. https://gitlab.com/kicad/code/kicad/-/issues/12613[#12613]
- https://gitlab.com/kicad/code/kicad/-/commit/bf37bd8ed510742d38a2a6812bb248bbb42f632b[Fix DRC error with arcs].
- Fix text position when drawing PCB text on non paired layers. https://gitlab.com/kicad/code/kicad/-/issues/18980[#18980]
- Fix error in DRC rule resolver. https://gitlab.com/kicad/code/kicad/-/issues/19012[#19012]
- Force orphaned nets to obey the minimum board clearance. https://gitlab.com/kicad/code/kicad/-/issues/19051[#19051]
- Fix Eagle footprint library migration crash. https://gitlab.com/kicad/code/kicad/-/issues/18702[#18702]
- Fix unconnected pads missing in IPC-2581 export. https://gitlab.com/kicad/code/kicad/-/issues/18293[#18293]
- Fix crash on routing many traces. https://gitlab.com/kicad/code/kicad/-/issues/18328[#18328]
- Correctly generate thermal reliefs on parts rotated by 45 degrees. https://gitlab.com/kicad/code/kicad/-/issues/18329[#18329]
- Fix circular courtyard DRC error. https://gitlab.com/kicad/code/kicad/-/issues/18347[#18347]
- Fix random malformed board outline with arcs DRC errors. https://gitlab.com/kicad/code/kicad/-/issues/18125[#18125]
- Properly group automatically generated objects. https://gitlab.com/kicad/code/kicad/-/issues/18874[#18874]
- Fixing paste issue in footprint library tables. https://gitlab.com/kicad/code/kicad/-/issues/18732[#18732]
- Fix disappearing courtyard layers. https://gitlab.com/kicad/code/kicad/-/issues/18978[#18978]
- https://gitlab.com/kicad/code/kicad/-/commit/beb30dd881c8465b14d65b07a186cd64aac7d85a[Use appropriate tuning pattern icon in menus for tuning pattern generators].
- Make convert to lines use the width of polygons where applicable. https://gitlab.com/kicad/code/kicad/-/issues/18713[#18713]
- Length tuning workflow improvements. https://gitlab.com/kicad/code/kicad/-/issues/18979[#18979]
- Show empty field for unconstrained length in length tuning properties. https://gitlab.com/kicad/code/kicad/-/issues/18979[#18979]
- Fix crash after Fabmaster import. https://gitlab.com/kicad/code/kicad/-/issues/19174[#19174]
- Update DRC exclusions from DRC dialog. https://gitlab.com/kicad/code/kicad/-/issues/19113[#19113]


=== Footprint Editor
- Fix crash when saving a footprint. https://gitlab.com/kicad/code/kicad/-/issues/19018[#19018]
- Add 3D mouse support for 3D view. https://gitlab.com/kicad/code/kicad/-/issues/14002[#14002]


=== Gerber Viewer
- Add 3D mouse support. https://gitlab.com/kicad/code/kicad/-/issues/13306[#13306]


=== 3D Viewer
- Ensure the 3D display view is not modified after menu selection. https://gitlab.com/kicad/code/kicad/-/issues/17422[#17422]
- Fix omission of STEP models with relative path assignment. https://gitlab.com/kicad/code/kicad/-/issues/16242[#16242]


=== Worksheet Editor
- Add 3D mouse support. https://gitlab.com/kicad/code/kicad/-/issues/13306[#13306]


=== PCB Calculator
- Always use black text in E-series resistor table. https://gitlab.com/kicad/code/kicad/-/issues/17033[#17033]


=== Command Line Interface
- Accept multiple Gerber layers for single layer plotting. https://gitlab.com/kicad/code/kicad/-/issues/19089[#19089]


=== macOS
- Fix missed rows when pasting to grid. https://gitlab.com/kicad/code/kicad/-/issues/18916[#18916]


=== Python Scripting
- Ensure image handlers are loaded before loading a board.  https://gitlab.com/kicad/code/kicad/-/issues/18959[#18959]
