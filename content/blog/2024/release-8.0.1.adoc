+++
title = "KiCad 8.0.1 Release"
date = "2024-03-15"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 8.0.1 bug fix release.
The 8.0.1 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 8.0.0 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/35[KiCad 8.0.1
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 8.0.1 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/8.0/[8.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix custom font rendering issue. https://gitlab.com/kicad/code/kicad/-/issues/11463[#11463]
- Improve custom font rendering performance. https://gitlab.com/kicad/code/kicad/-/issues/16568[#16568]
- Update properties panel to use select language. https://gitlab.com/kicad/code/kicad/-/issues/16589[#16589]
- Make double click open object properties dialog in search panel. https://gitlab.com/kicad/code/kicad/-/issues/16934[#16934]
- https://gitlab.com/kicad/code/kicad/-/commit/2ab492474e2983ee218ec711a0e769a3cb8e590d[Improve object alignment tooltips].
- https://gitlab.com/kicad/code/kicad/-/commit/6feafedfbe93464162a685e87cff3eab034b5c3f[Use correct text color in library tree selected items on non-OSX platforms].
- Fix floating point rounding issues with scaled parameters in settings. https://gitlab.com/kicad/code/kicad/-/issues/17070[#17070]
- Fix bottom panel background color in footprint and symbol chooser frames. https://gitlab.com/kicad/code/kicad/-/issues/15810[#15810]
- Don't import invalid SVG polygons. https://gitlab.com/kicad/code/kicad/-/issues/17091[#17091]
- https://gitlab.com/kicad/code/kicad/-/commit/5618a6925d002107d97db8f0c21186f45a618cf7[Improve fallback (Cairo) canvas performance].
- https://gitlab.com/kicad/code/kicad/-/commit/d359fb8ab90937d3a5b4dc4cfbabf7dc04da2952[Reduce fallback (Cairo) canvas memory use].
- Check environment variable when testing for unresolved variables. https://gitlab.com/kicad/code/kicad/-/issues/17174[#17174]
- Use a version string without the extra packaging information in drawing sheets. https://gitlab.com/kicad/code/kicad/-/issues/17176[#17176]
- Don't show hidden directories and files in the project tree browser. https://gitlab.com/kicad/code/kicad/-/issues/16207[#16207]


=== Schematic Editor
- Fix ghosting when placing multiple label and text objects. https://gitlab.com/kicad/code/kicad/-/issues/16885[#16885]
- https://gitlab.com/kicad/code/kicad/-/commit/7ae14f963035d2c914a2fe3e0a9654108e7b6517[Improve custom rule `memberOfFootprint` help].
- https://gitlab.com/kicad/code/kicad/-/commit/152c458b9c9e311d4e903c4026e7be512851a063[Improve custom rule `assertion` constraints help].
- Do not prevent netclass directive label from being placed when previously placed with no netclass assignment. https://gitlab.com/kicad/code/kicad/-/issues/16997[#16997]
- Do not reset annotation on reference changes when updating or changing symbols from library. https://gitlab.com/kicad/code/kicad/-/issues/16991[#16991]
- Improve layout of schematic colors preview. https://gitlab.com/kicad/code/kicad/-/issues/17043[#17043]
- Set up default netclass wire and bus widths when importing Altium schematic. https://gitlab.com/kicad/code/kicad/-/issues/17024[#17024]
- Ignore field moves when comparing schematic symbol to library symbol. https://gitlab.com/kicad/code/kicad/-/issues/16944[#16944]
- Change symbol library search to "and" instead of "or" search terms. https://gitlab.com/kicad/code/kicad/-/issues/16974[#16974]
- Do not load pin properties as fields when importing Altium. https://gitlab.com/kicad/code/kicad/-/issues/17048[#17048]
- https://gitlab.com/kicad/code/kicad/-/commit/a13e4e7d9f033bf06d6d976abbda7bcd5debf38c[Select graphical shapes based on effective shape instead of bounding box].
- https://gitlab.com/kicad/code/kicad/-/commit/ffd470d94d4ea0d5061e695d57511ed3390e5b2a[Improve connectivity performance].
- https://gitlab.com/kicad/code/kicad/-/commit/339646a267df034ca2f53537b5fc13bd3275b43c[Improve responsiveness of some editing operations].
- Fix crash when assigning footprint in symbol properties dialog.  https://gitlab.com/kicad/code/kicad/-/issues/17154[#17154]
- Show correct library identifier in change symbols dialog. https://gitlab.com/kicad/code/kicad/-/issues/17162[#17162]
- Keep schematic text upright when opening V6 schematic. https://gitlab.com/kicad/code/kicad/-/issues/17082[#17082]
- Fix sheet pin align to grid issue. https://gitlab.com/kicad/code/kicad/-/issues/16920[#16920]
- https://gitlab.com/kicad/code/kicad/-/commit/b07d43a24a8036e880aa4b5855ecaddb525245ac[Add text size to symbol field properties panel].
- Do not draw checkboxes and icons on top of each other in footprint chooser. https://gitlab.com/kicad/code/kicad/-/issues/17217[#17217]
- Do not overwrite the first item in alternate pin selection list when changing alternate pin. https://gitlab.com/kicad/code/kicad/-/issues/17221[#17221]
- Fix crash when editing a legacy schematic with missing library symbols. https://gitlab.com/kicad/code/kicad/-/issues/17232[#17232]
- Prevent silent deletion of entire symbol when using "Edit Symbol Fields" dialog. https://gitlab.com/kicad/code/kicad/-/issues/17229[#17229]
- Do not print selected item on every page. https://gitlab.com/kicad/code/kicad/-/issues/17132[#17132]
- Update net navigator when cross probing from board editor net highlight. https://gitlab.com/kicad/code/kicad/-/issues/16838[#16838]
- Convert legacy schematic descriptions to fields. https://gitlab.com/kicad/code/kicad/-/issues/17202[#17202]
- Fix crash in zone filler. https://gitlab.com/kicad/code/kicad/-/issues/17237[#17237]
- Fix crash when ignoring an ERC violation. https://gitlab.com/kicad/code/kicad/-/issues/17375[#17375]


=== Spice Simulator
- Improve tooltips. https://gitlab.com/kicad/code/kicad/-/issues/17009[#17009]
- Update VDMOS model defaults to ngspice-41 and later. https://gitlab.com/kicad/code/kicad/-/issues/17073[#17073]
- https://gitlab.com/kicad/code/kicad/-/commit/14d71c8ca6b48d2eb956bb069acf05a37b1b2652[Fix collecting of signals for S-parameter analysis].
- Do not use ohms as units for controlling voltage source parameter. https://gitlab.com/kicad/code/kicad/-/issues/17076[#17076]
- Handle single source DC analyses correctly. https://gitlab.com/kicad/code/kicad/-/issues/17119[#17119]
- Only change pin assignments from user interaction. https://gitlab.com/kicad/code/kicad/-/issues/16349[#16349]


=== Symbol Editor
- Update description in library tree immediately when changed in properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/16971[#16971]
- Improve database library load performance. https://gitlab.com/kicad/code/kicad/-/issues/17031[#17031]
- Add pin count and footprint filters options to footprint chooser dialog. https://gitlab.com/kicad/code/kicad/-/issues/17105[#17105]
- Add show/hide invisible pins and fields to view menu and toolbar. https://gitlab.com/kicad/code/kicad/-/issues/8020[#8020]
- Add KiCad symbol library files to import symbol file selector filters. https://gitlab.com/kicad/code/kicad/-/issues/17176[#17176]
- Fix crash when switching between two git branches of an open library. https://gitlab.com/kicad/code/kicad/-/issues/17204[#17204]


=== Board Editor
- Properly create bounding hull cutouts for rule area zones. https://gitlab.com/kicad/code/kicad/-/issues/16347[#16347]
- Do not snap mirrored graphic objects to center of selection when not moved. https://gitlab.com/kicad/code/kicad/-/issues/16524[#16524]
- Prevent unnecessary move at the beginning of move command. https://gitlab.com/kicad/code/kicad/-/issues/11080[#11080]
- Prevent duplicate nets when renaming a net in the net inspector. https://gitlab.com/kicad/code/kicad/-/issues/17025[#17025]
- Rule areas should be allowed on user layers. https://gitlab.com/kicad/code/kicad/-/issues/17034[#17034]
- Update "Edit Teardrops" dialog to match other teardrop terminology. https://gitlab.com/kicad/code/kicad/-/issues/17050[#17050]
- Improve teardrop dialog terminology. https://gitlab.com/kicad/code/kicad/-/issues/17050[#17050]
- https://gitlab.com/kicad/code/kicad/-/commit/ef6a5b625237c3053462f15d2a1e4ef1f470b9c1[Fix teardrop generation when arc track is connected to pad].
- https://gitlab.com/kicad/code/kicad/-/commit/711c6141a85c9e6a42c8025925fe27ae7660044c[Highlight nets and show length tuning status when adding tuning patterns].
- Add turn off all layers preset option to the appearance manager. https://gitlab.com/kicad/code/kicad/-/issues/15625[#15625]
- Fix assert and ghost footprint after undoing a footprint drag operation. https://gitlab.com/kicad/code/kicad/-/issues/17087[#17087]
- Fix broken routing tool immediate mode (X key press) handling. https://gitlab.com/kicad/code/kicad/-/issues/17101[#17101]
- Fix broken rotated rectangles when importing Eagle boards. https://gitlab.com/kicad/code/kicad/-/issues/17135[#17135]
- Do not unexpectedly switch focus in board setup grids using the up and down arrow keys. https://gitlab.com/kicad/code/kicad/-/issues/17144[#17144]
- Improve grid origin action discoverability. https://gitlab.com/kicad/code/kicad/-/issues/17007[#17007]
- Fix crash when placing vias and using measure tool. https://gitlab.com/kicad/code/kicad/-/issues/17164[#17164]
- Add option to footprint chooser to switch between the selected footprint or its 3D model. https://gitlab.com/kicad/code/kicad/-/issues/16173[#16173]
- https://gitlab.com/kicad/code/kicad/-/commit/f45df48498a6a5257c24a3bbbbc541550e25207a[Fix memory leak in net inspector dialog].
- Fix memory leak when adding tracks. https://gitlab.com/kicad/code/kicad/-/issues/16839[#16839]
- Reduce undo buffer memory usage when deleting group objects. https://gitlab.com/kicad/code/kicad/-/issues/17175[#17175]
- Support multi-line text when importing EasyEDA boards. https://gitlab.com/kicad/code/kicad/-/issues/17171[#17171]
- https://gitlab.com/kicad/code/kicad/-/commit/985c3ad92a4eac39cd673bfb14363e9638312ce7[Fix issue with rounded rectangles in GERBER plots].
- Unflip view when selecting a viewport which isn't flipped. https://gitlab.com/kicad/code/kicad/-/issues/13773[#13773]
- Prevent tool re-entrancy when placing vias and using measure tool. https://gitlab.com/kicad/code/kicad/-/issues/17164[#17164]
- Do not block DRC when schematic parity is check is performed. https://gitlab.com/kicad/code/kicad/-/issues/17003[#17003]
- Do not extend lines beyond the usable canvas. https://gitlab.com/kicad/code/kicad/-/issues/16959[#16959]
- Fix crash loading board with undefined net item information. https://gitlab.com/kicad/code/kicad/-/issues/17230[#17230]
- https://gitlab.com/kicad/code/kicad/-/commit/25b033df2a15892538338363bac8f9d2ed22d80a[Rework on bottom panel of footprint chooser to properly display long strings].
- Correctly handle circular board outline cut outs in STEP exports. https://gitlab.com/kicad/code/kicad/-/issues/17137[#17137]
- Correctly handle footprint graphic knockouts for no-net case. https://gitlab.com/kicad/code/kicad/-/issues/17223[#17223]
- Save and restore corner radius ratio when plotting pads. https://gitlab.com/kicad/code/kicad/-/issues/17247[#17247]
- Don't select reference images when they are hidden. https://gitlab.com/kicad/code/kicad/-/issues/16283[#16283]
- Revert changed rotation behavior for all objects except text boxes. https://gitlab.com/kicad/code/kicad/-/issues/16481[#16481]
- Fix selection freeze when selecting two zones on the same layer with different nets. https://gitlab.com/kicad/code/kicad/-/issues/17327[#17327]
- Do not clip output when plotting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/17355[#17355]


=== Footprint Editor
- Improve missing feature warnings when importing Altium footprint libraries. https://gitlab.com/kicad/code/kicad/-/issues/16975[#16975]
- Draw graphical shapes with correct fill color. https://gitlab.com/kicad/code/kicad/-/issues/17250[#17250]


=== 3D Viewer
- Add option to use PCB Editor physical stackup settings as defaults. https://gitlab.com/kicad/code/kicad/-/issues/17085[#17085]
- Update color changes made in appearance panel. https://gitlab.com/kicad/code/kicad/-/issues/17374[#17374]


=== Gerber Viewer
- Fix broken XOR mode behavior. https://gitlab.com/kicad/code/kicad/-/issues/17282[#17282]


=== Worksheet Editor
- Hide text item properties built in keywords drop down list when changing control focus. https://gitlab.com/kicad/code/kicad/-/issues/17124[#17124]
- https://gitlab.com/kicad/code/kicad/-/commit/8a66a7eab0dd43775ffc0cbe7e6e6eeb976e841d[Fix properties frame focus issue].


=== Python scripting
- Fix broken Python path on Debian based platforms. https://gitlab.com/kicad/code/kicad/-/issues/16939[#16939] and https://gitlab.com/kicad/code/kicad/-/issues/17151[#17151]


=== Command Line Interface
- https://gitlab.com/kicad/code/kicad/-/commit/f7ddd211d9085131fafabed9aa848c98f1e5b99d[Allow upgrade of legacy and non-KiCad symbol libraries].


=== Windows
- Fix footprint chooser graphical glitch after task switch. https://gitlab.com/kicad/code/kicad/-/issues/15810[#15810]
- Fix unselectable footprints in footprint chooser. https://gitlab.com/kicad/code/kicad/-/issues/15889[#15889]
- Fix crash on pre-1903 Chinese Windows 10 systems when importing EasyEDA projects. https://gitlab.com/kicad/code/kicad/-/issues/17248[#17248]


=== macOS
- Fix memory allocation crashes. https://gitlab.com/kicad/code/kicad/-/issues/16998[#16998]
- Fix file tree icons. https://gitlab.com/kicad/code/kicad/-/issues/17265[#17265]


=== Linux
- https://gitlab.com/kicad/code/kicad/-/commit/7eeb1a19cfce8733e3882cb1338c9ec333f49908[Fix net selector filter field focus on GTK].
- Force correct library tree row height on GTK. https://gitlab.com/kicad/code/kicad/-/issues/16403[#16403]
