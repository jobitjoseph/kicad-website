+++
title = "2024 End of Year Fund Drive"
date = "2024-12-18"
draft = false
"blog/categories" = [
    "News"
]
+++

The end of the year is approaching, and the KiCad project is excited to announce our
https://donate.kicad.org[**2024 End of Year Fund Drive**].  With the release of KiCad version 9.0 just around the corner,
we are looking forward to another successful year of development. Our supporters'
contributions have been instrumental in building the features of version 9, and we are
counting on your continued support to make version 9 even better! Our year-end donation
campaign begins this year on January 10th.

Direct funding of the KiCad project is crucial for the development of new features,
bug fixes, and improvements to the user experience. Your donations enable us to maintain
the infrastructure that keeps the project running, including the website, forum,
bug tracker, and continuous integration system. Sustaining donors, who provide ongoing
monthly support, are especially vital to our success.

This year, we are thrilled to offer a new KiCad sponsor T-shirt design to our sustaining donors.
We've upgraded the t-shirt shirt material to soft, organic, 100% cotton in both
men's and women's styles. Monthly donors are eligible to receive a shirt after
contributing a total of at least 60 USD in 2024.

We are also providing this sponsor t-shirt to our donors who have generously offered to match
your donations for the first 10 days of the funding drive.  We are grateful for their support
and encourage you to take advantage of this opportunity to double the impact of your donation.

image::/img/blog/2024/2025_sponsor_tshirt.jpg[400,400, title="Monthly Donor T-Shirt"]

If your company would like to sponsor matching donations during the 2024 funding drive,
please contact us at mailto:sponsorship@kicad.org[sponsorship@kicad.org].