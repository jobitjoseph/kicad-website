+++
title = "KiCad 8.0.3 Release"
date = "2024-06-04"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 8.0.3 bug fix release.
The 8.0.3 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 8.0.2 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/37[KiCad 8.0.3
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 8.0.3 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/8.0/[8.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Property grid navigation improvements. https://gitlab.com/kicad/code/kicad/-/issues/16957[#16957]
- Make sure color settings are saved when modified. https://gitlab.com/kicad/code/kicad/-/issues/17864[#17864]
- Improve update notice dialog tooltip. https://gitlab.com/kicad/code/kicad/-/issues/17927[#17927]
- https://gitlab.com/kicad/code/kicad/-/commit/b8fa10ab2b3685309639e14ad1cd6e37a6b497dd[Add optional reporting of non-KiCad design import issues].
- https://gitlab.com/kicad/code/kicad/-/commit/c7dd99343916cae6adc92b5ac2ccf43b3c5d83ad[Add OpenGL info to version information].
- Fix a 100% CPU core usage is some editing cases. https://gitlab.com/kicad/code/kicad/-/issues/17979[#17979]
- Fix issues with non-kicad fonts. https://gitlab.com/kicad/code/kicad/-/issues/18014[#18014]
- Handle font when reading render cache. https://gitlab.com/kicad/code/kicad/-/issues/17666[#17666]


=== Schematic Editor
- Improve symbol instance data file save ordering. https://gitlab.com/kicad/code/kicad/-/issues/17737[#17737]
- Do not show click to start wire cursor for hidden pins. https://gitlab.com/kicad/code/kicad/-/issues/17145[#17145]
- Internationalize pin information when language changes. https://gitlab.com/kicad/code/kicad/-/issues/17224[#17224]
- Update status bar and properties panel after edit field properties dialog is dismissed. https://gitlab.com/kicad/code/kicad/-/issues/17696[#17696]
- Prevent symbol browser from deselecting currently active item. https://gitlab.com/kicad/code/kicad/-/issues/17808[#17808]
- Use correct value field text when importing Eagle schematic symbols. https://gitlab.com/kicad/code/kicad/-/issues/17130[#17130]
- Make pin helpers available for hierarchical sheet pins. https://gitlab.com/kicad/code/kicad/-/issues/17923[#17923]
- https://gitlab.com/kicad/code/kicad/-/commit/90b62762d3c16b49343eccd4921b94ea90fb3fc3[Support orthoganol dragging labels of sheet pins].
- Import EasyEDA symbol meta data. https://gitlab.com/kicad/code/kicad/-/issues/17806[#17806]
- Fix crash when loading symbols from  database library.  https://gitlab.com/kicad/code/kicad/-/issues/17903[#17903]
- Fix renaming sheet check. https://gitlab.com/kicad/code/kicad/-/issues/17901[#17901]
- Fill entire table if cache is empty when loading one part from database library. https://gitlab.com/kicad/code/kicad/-/issues/17940[#17940]
- Automatically save new hierarchical sheets. https://gitlab.com/kicad/code/kicad/-/issues/17810[#17810]
- Remember BOM export file name per project. https://gitlab.com/kicad/code/kicad/-/issues/17704[#17704]
- Fix editing line angle rotation. https://gitlab.com/kicad/code/kicad/-/issues/17473[#17473]
- Use consistent sorting for footprint libraries in footprint assignment tool. https://gitlab.com/kicad/code/kicad/-/issues/17731[#17731]
- Fix incorrect label orientation when the symbol is rotated. https://gitlab.com/kicad/code/kicad/-/issues/18012[#18012]
- Fix crash when editing text properties via properties panel. https://gitlab.com/kicad/code/kicad/-/issues/18016[#18016]
- https://gitlab.com/kicad/code/kicad/-/commit/e20a11cbb31a311b0c57db0c0ea29c7b904cc3df[Fix selection of items inside filled shapes].
- https://gitlab.com/kicad/code/kicad/-/commit/dc08cf32526158d74ca2ccc3753c4b7c9baac5e3[Make 'note' backgrounds translucent on selection].


=== Spice Simulator
- Fix internal simulation errors when using some IBIS models. https://gitlab.com/kicad/code/kicad/-/issues/17701[#17701]
- Send project path to ngspice for code model input files. https://gitlab.com/kicad/code/kicad/-/issues/16527[#16527]
- Don't add duplicate simulation fields. https://gitlab.com/kicad/code/kicad/-/issues/17970[#17970]
- Update signals and measurements when netlist might have changed. https://gitlab.com/kicad/code/kicad/-/issues/17616[#17616]
- Use display titles for axes in CSV output. https://gitlab.com/kicad/code/kicad/-/issues/17324[#17324]
- https://gitlab.com/kicad/code/kicad/-/commit/c3e7509823d1a3728a03857ba6f4b082b53272b9[Allow setting field size with property manager].
- Fix incorrect netlist for uniform random voltage sources. https://gitlab.com/kicad/code/kicad/-/issues/16393[#16393]
- Allow ".ends" command to be preceeded by whitespace. https://gitlab.com/kicad/code/kicad/-/issues/16560[#16560]
- Support multiple brace-expressions for coupled multi-conductor line model syntax. https://gitlab.com/kicad/code/kicad/-/issues/17824[#17824]
- Make IBIS errors visible and more obvious. https://gitlab.com/kicad/code/kicad/-/issues/18041[#18041]


=== Symbol Editor
- Apply power filter to Altium symbol libraries. https://gitlab.com/kicad/code/kicad/-/issues/17922[#17922]
- Select pin when clicking on electrical type text. https://gitlab.com/kicad/code/kicad/-/issues/16183[#16183]
- Fix crash when canceling footprint library table dialog and the footprint library file is missing. https://gitlab.com/kicad/code/kicad/-/issues/17989[#17989]


=== Board Editor
- https://gitlab.com/kicad/code/kicad/-/commit/8edfc23679ee36142b15523d8d27585bfbe50db9[Pad thermal spoke angle is 90 degrees by default when importing Altium PCB].
- Do not require second mouse click to reflect changes after updating custom footprint. https://gitlab.com/kicad/code/kicad/-/issues/17493[#17493]
- Allow no-net teardrops on no-net pads with no-net tracks. https://gitlab.com/kicad/code/kicad/-/issues/17566[#17566]
- Prevent hidden shapes from being selected. https://gitlab.com/kicad/code/kicad/-/issues/17562[#17562]
- Support legacy length tuning settings workflow. https://gitlab.com/kicad/code/kicad/-/issues/17748[#17748]
- Properly handle DNP and exclude from board components in net list. https://gitlab.com/kicad/code/kicad/-/issues/17146[#17146]
- Do no generate DRC warning on "exclude from position file" property. https://gitlab.com/kicad/code/kicad/-/issues/17819[#17819]
- Fix incorrect size of widgets when a very long field name exists in IPC-2581 export dialog. https://gitlab.com/kicad/code/kicad/-/issues/17911[#17911]
- Fix version 7 imported board hidden description field positions. https://gitlab.com/kicad/code/kicad/-/issues/17684[#17684]
- Re-allow moving footprint text. https://gitlab.com/kicad/code/kicad/-/issues/17899[#17899]
- Keep footprint chooser on top of KiCad but not system. https://gitlab.com/kicad/code/kicad/-/issues/16840[#16840]
- Use StandardPrimitive library for vias when exporting IPC-2581. https://gitlab.com/kicad/code/kicad/-/issues/16661[#16661]
- DRC via diameter calculation disregards PCB pad overrides and throws annular width error. https://gitlab.com/kicad/code/kicad/-/issues/17944[#17944]
- Fix pasting footprint references. https://gitlab.com/kicad/code/kicad/-/issues/17937[#17937]
- Improve consistency of free via ambiguity tests. https://gitlab.com/kicad/code/kicad/-/issues/17671[#17671]
- Handle orientation correctly when pasting footprint items. https://gitlab.com/kicad/code/kicad/-/issues/17518[#17518]
- Get rid of relative position in pad properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/17246[#17246]
- Correctly mirror footprint fields on load. https://gitlab.com/kicad/code/kicad/-/issues/17955[#17955]
- Fix wrong file name for placement files when opening another project. https://gitlab.com/kicad/code/kicad/-/issues/17846[#17846]
- Show disambiguated net name when needed. https://gitlab.com/kicad/code/kicad/-/issues/16187[#16187]
- Fix false annular ring width DRC test failure. https://gitlab.com/kicad/code/kicad/-/issues/17485[#17485]
- Fix false DRC errors for some rounded pad corners on unmodified footprints. https://gitlab.com/kicad/code/kicad/-/issues/17249[#17249]
- Add mirrored column to fields grid table. https://gitlab.com/kicad/code/kicad/-/issues/17956[#17956]
- Expose violation severity for co-located holes. https://gitlab.com/kicad/code/kicad/-/issues/18028[#18028]
- Fix crash with some 3D models. https://gitlab.com/kicad/code/kicad/-/issues/17950[#17950]
- Do not faile opening files with rotated text boxes. https://gitlab.com/kicad/code/kicad/-/issues/18033[#18033]
- Test library identifiers in "intersectsCourtyard" and "insideCourtyard" custom rules. https://gitlab.com/kicad/code/kicad/-/issues/18039[#18039]
- Avoid crash if zone has no fill when importing Altium board.  https://gitlab.com/kicad/code/kicad/-/issues/18025[#18025]
- Fix broken hit test for arcs in via placer. https://gitlab.com/kicad/code/kicad/-/issues/17844[#17844]
- Fix crash when dragging overlapping vias. https://gitlab.com/kicad/code/kicad/-/issues/17915[#17915]
- Fix crash after undoing edits after reducing number of layers in board stackup. https://gitlab.com/kicad/code/kicad/-/issues/17613[#17613]
- Add missing holes from STEP export with circular PCB outline. https://gitlab.com/kicad/code/kicad/-/issues/17446[#17446]
- Fix broken and/or disconnected PNS router traces in tight areas. https://gitlab.com/kicad/code/kicad/-/issues/16591[#16591]
- https://gitlab.com/kicad/code/kicad/-/commit/c248993a843da28d4bfe367f8e74eb40637297c2[Avoid snapping to edge cuts when routing].
- https://gitlab.com/kicad/code/kicad/-/commit/be2f317f6d970c3d1a62fcfb09963f17c15907df[Vertically center the color swatch in grid layer selector].
- Fix variable expansion issue when exporting STEP of a modified board file. https://gitlab.com/kicad/code/kicad/-/issues/16973[#16973]
- Reset zone offsets when exporting footprints. https://gitlab.com/kicad/code/kicad/-/issues/17794[#17794]
- https://gitlab.com/kicad/code/kicad/-/commit/36119b869c49e2b92e46f79506ffcbde281ca587[Fix context menu when moving].
- Enable moving of all footprint text. https://gitlab.com/kicad/code/kicad/-/issues/18024[#18024]
- https://gitlab.com/kicad/code/kicad/-/commit/1073ca07600fdab7d2e7af68b7587350fc64fe93[Avoids rounding errors flagging connections too small by nanometers in DRC connection width check].
- Fix very tall plot dialog on GTK. https://gitlab.com/kicad/code/kicad/-/issues/16926[#16926]


=== Footprint Editor
- Allow specifying field layer in footprint properties. https://gitlab.com/kicad/code/kicad/-/issues/17758[#17758]
- Don't modify circle radius when editing center point. https://gitlab.com/kicad/code/kicad/-/issues/17192[#17192]
- Prevent always writing the footprint field on save. https://gitlab.com/kicad/code/kicad/-/issues/17998[#17998]


=== 3D Viewer
- Fix orientations of some footprint STEP models. https://gitlab.com/kicad/code/kicad/-/issues/17631[#17631]
- Fix missing parts in STEP models. https://gitlab.com/kicad/code/kicad/-/issues/14906[#14906]
- Fix difference between STEP export and 3D view with some STEP models. https://gitlab.com/kicad/code/kicad/-/issues/18030[#18030]


=== Gerber Viewer
- Force "always hide all layers but active" update when using hotkey. https://gitlab.com/kicad/code/kicad/-/issues/18059[#18059]


=== Worksheet Editor
- Fix broken field text editors. https://gitlab.com/kicad/code/kicad/-/issues/17284[#17284]


=== Command Line Interface
- Honour LAYER, SHEETNAME and SHEETPATH variable overrides. https://gitlab.com/kicad/code/kicad/-/issues/17680[#17680]
- Center symbol SVG export. https://gitlab.com/kicad/code/kicad/-/issues/18062[#18062]


=== PCB Calculator
- Correctly handle language changes. https://gitlab.com/kicad/code/kicad/-/issues/17825[#17825]


=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/aea70a14ff1169060b9a87990ef74434c30128be[Fix win32 proxy parsing when the old IE options for scheme based configuration was set].


=== macOS
- Fix to keep ERC/DRC on top. https://gitlab.com/kicad/code/kicad/-/issues/14356[#14356]
- Fix memory use after free crash in standalone schematic editor. https://gitlab.com/kicad/code/kicad/-/issues/17369[#17369]


=== Linux
- https://gitlab.com/kicad/code/kicad/-/commit/87f3d451092388c295f808b886576939d7bbc14d[Require Wayland 1.20 libraries].
