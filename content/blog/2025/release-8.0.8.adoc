+++
title = "KiCad 8.0.8 Release"
date = "2025-01-03"
draft = true
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 8.0.8 bug fix release.
The 8.0.8 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 8.0.8 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/42[KiCad 8.0.8
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 8.0.8 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/8.0/[8.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Remove compensation padding in KiCad launcher for newer wxWidgets versions. https://github.com/wxWidgets/wxWidgets/issues/24550[#24550]
- Make settings migration dialog re-sizable. https://gitlab.com/kicad/code/kicad/-/issues/19347[#19347]
- Fix parsing of Altium overbar notation. https://gitlab.com/kicad/code/kicad/-/issues/19080[#19080]
- Update pin table when model changes. https://gitlab.com/kicad/code/kicad/-/issues/19253[#19253]
- Recognize ${KICAD7_3DMODEL_DIR} and all future major releases. https://gitlab.com/kicad/code/kicad/-/issues/18809[#18809]


=== Schematic Editor
- Plot labels in color. https://gitlab.com/kicad/code/kicad/-/issues/19040[#19040]
- https://gitlab.com/kicad/code/kicad/-/commit/2e023fcc99d82d4c522a03376988786c670b10b6[Fix some issues that can crash editor on close].
- Don't change selection after selecting find-next hit. https://gitlab.com/kicad/code/kicad/-/issues/19233[#19233]
- Display footprints in footprint  assignment tool. https://gitlab.com/kicad/code/kicad/-/issues/18685[#18685]
- https://gitlab.com/kicad/code/kicad/-/commit/66b392cdcabdea7f470daa6dcfebeffef2d6f45a[Avoid crash on save if a polyline is empty].
- Don't rename root sheet when switching language. https://gitlab.com/kicad/code/kicad/-/issues/19262[#19262]
- Do not save automatically placed flag for text with no fields. https://gitlab.com/kicad/code/kicad/-/issues/19159[#19159]
- Fix connectivity issue with global label connected to power symbol. https://gitlab.com/kicad/code/kicad/-/issues/18092[#18092]
- Preserve symbol unit when loading schematic with missing instance data. https://gitlab.com/kicad/code/kicad/-/issues/19073[#19073]
- Fix crash in schematic editor when finishing a wire. https://gitlab.com/kicad/code/kicad/-/issues/18136[#18136]
- Fix crash when opening footprint browser from schematic symbol properties. https://gitlab.com/kicad/code/kicad/-/issues/19318[#19318]
- Fix QA test failure in orcadpcb2 netlist exporter. https://gitlab.com/kicad/code/kicad/-/issues/18822[#18822]
- Fix a crash when library symbol is missing. https://gitlab.com/kicad/code/kicad/-/issues/17146[#17146]
- Fix crash after project save and reopened after cannot find symbol in netlist error. https://gitlab.com/kicad/code/kicad/-/issues/17146[#17146]
- Improve symbol load time when opening a large database library. https://gitlab.com/kicad/code/kicad/-/issues/18826[#18826]
- Fix crash when undoing new sheet action from within sheet. https://gitlab.com/kicad/code/kicad/-/issues/19358[#19358]


=== Symbol Editor
- Fix crash on corrupt symbol library table. https://gitlab.com/kicad/code/kicad/-/issues/19236[#19236]
- Don't hang when trying to paste image as text. https://gitlab.com/kicad/code/kicad/-/issues/19079[#19079]
- Allow opening color picker dialog from polygon shape properties panel. https://gitlab.com/kicad/code/kicad/-/issues/19254[#19254]


=== Spice Simulator
- Only populate spice code in the simulation dialog code field. https://gitlab.com/kicad/code/kicad/-/issues/18771[#18771]
- Fix crash after probing and changing signal color. https://gitlab.com/kicad/code/kicad/-/issues/17841[#17841]


=== Board Editor
- https://gitlab.com/kicad/code/kicad/-/commit/6a359eb43335cfc9c93d6a91d4d0f6bb11df51a0[Handle circular zone cutouts in Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/7888eb768e258bff5f7ab01a7e5f23f19f0873fa[Don't add zero radius holes on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/4ceccf4c479ad6bb7f8c589a6a24dc838bb39302[Do not import unplaced footprints on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/c1630ee0f19579f6b9b52eb96efc7190065c5a2f[Handle circles on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/e94ea208ab5c90255f6ea757d8daaa5bc436906f[Map unknown layers to KiCad user layers on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/9ce74a8807c568e944b1db386391f49b089d5661[Handle rectangles on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/12098f4a3c54edfa81234375c269f3211b465012[Improve detection of open versus closed polygons on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/0d3208e656552888d83212b1cbd64ce1270b2a0e[Fix text import on Fabmaster import].
- https://gitlab.com/kicad/code/kicad/-/commit/6bbbc07998fc18f2a04ab72a4820dfcee37536f6[Handle squares, oblongs, and regular polygons on Fabmaster import].
- Keep DRC marker exclusions up to date. https://gitlab.com/kicad/code/kicad/-/issues/17429[#17429]
- Make sure descendants get new UUIDs when pasting. https://gitlab.com/kicad/code/kicad/-/issues/19052[#19052]
- Fix crash when running DRC. https://gitlab.com/kicad/code/kicad/-/issues/18600[#18600]
- Allow plot options to override the board tenting settings. https://gitlab.com/kicad/code/kicad/-/issues/18991[#18991]
- Add example for `silk_board_edge_clearance` custom rule. https://gitlab.com/kicad/code/kicad/-/issues/19260[#19260]
- Fix rats nest issue where blind vias are treated as through vias. https://gitlab.com/kicad/code/kicad/-/issues/18982[#18982]
- Don't trigger DRC footprint library warning when description and keywords do not match. https://gitlab.com/kicad/code/kicad/-/issues/19259[#19259]
- Allow selection of PCB DRC markers when not on active layer. https://gitlab.com/kicad/code/kicad/-/issues/19258[#19258]
- Allow DRC markers to be selected with area selection. https://gitlab.com/kicad/code/kicad/-/issues/19258[#19258]
- Rebuild layer preset list on language change. https://gitlab.com/kicad/code/kicad/-/issues/19181[#19181]
- Fix track start property display. https://gitlab.com/kicad/code/kicad/-/issues/19215[#19215]
- Fix thread deadlock when closing the footprint chooser dialog. https://gitlab.com/kicad/code/kicad/-/issues/18107[#18107]
- Prevent DRC freeze when using footprint with custom pad shapes. https://gitlab.com/kicad/code/kicad/-/issues/19325[#19325]
- Plot from render cache if fonts aren't embedded. https://gitlab.com/kicad/code/kicad/-/issues/18672[#18672]


=== Footprint Editor
- Properly handle group duplicate. https://gitlab.com/kicad/code/kicad/-/issues/19245[#19245]
- Ignore invisible pads in enumeration tool. https://gitlab.com/kicad/code/kicad/-/issues/18750[#18750]
- Do not add extra grid rows when resetting default field property preferences. https://gitlab.com/kicad/code/kicad/-/issues/19369[#19369]


=== Gerber Viewer


=== 3D Viewer
- Use font render cache when available. https://gitlab.com/kicad/code/kicad/-/issues/18672[#18672]


=== Worksheet Editor


=== PCB Calculator


=== Command Line Interface


=== macOS
- Fix dialog focus issue. https://gitlab.com/kicad/code/kicad/-/issues/3765[#3765] https://gitlab.com/kicad/code/kicad/-/issues/17460[#17460]


=== Python Scripting
