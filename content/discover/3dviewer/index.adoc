+++
title = "3D PCB Viewer Features"
tags = ["3d pcb viewer"
        ]
[menu.main]
    parent = "Discover"
    name   = "3D Viewer"
    weight = 3
+++

View your PCB designs in 3D with KiCad's integrated 3D Viewer! Coupled with a large library of
3D models, you can instantly inspect your design in an interactive 3D view.
Rotate and pan around to inspect details easier than with a 2D display.
<!--more-->

== Integrated with the PCB Editor

Easily launch the 3D Viewer for your active project from the PCB Editor

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline preload="metadata" class="embed-responsive-item">
        <source src="launch-from-pcb.webm" type="video/webm">
        <source src="launch-from-pcb.mp4" type="video/mp4">
    </video>
</div>
++++

== Ray tracing Renderer

Enable the ray tracing renderer to get more lighting accurate 3d views.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="raytracing.webm" type="video/webm">
        <source src="raytracing.mp4" type="video/mp4">
    </video>
</div>
++++

== Synchronized Update

The rendered image is capable of easily updating as you make changes to the PCB Layout.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="sync.webm" type="video/webm">
        <source src="sync.mp4" type="video/mp4">
    </video>
</div>
++++

== Customize an array of rendering settings

Change board color, lighting properties and more.

++++
<div class="text-center ratio ratio-16x9">
    <video autoplay loop muted playsinline class="embed-responsive-item">
        <source src="render-options.webm" type="video/webm">
        <source src="render-options.mp4" type="video/mp4">
    </video>
</div>
++++
