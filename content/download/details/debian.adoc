+++
title = "Install on Debian"
summary = "Install instructions for KiCad on Debian"
+++

== Basic Information

The information further down about the available KiCad packages might not
always be fully up to date and correct due ongoing development. For getting an
overview about the current most recent version for the KiCad packages within
the various Debian releases please have a look at
https://packages.debian.org/search?lang=en&keywords=kicad[packages.debian.org].

Please note that Debian stable releases don't get updates for existing
packages except security updates. Packages within the Stable releases usually
get newer and recent versions through backports repositories. KiCad is
here no special case.

To be able to install versions from a official Debian Backport repository you
need to extend your `sources.list` configuration if not yet happen. If you are
running Debian Bullseye please create a file
`/etc/apt/sources.list.d/bullseye-backports.list` if you don't have an similar
entry already.

[source,bash]
----
# /etc/apt/sources.list.d/bullseye-backports.list
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
----

If your system is still running Debian Buster create a similar file (if not
done somehow already or created by the installer similar)
`/etc/apt/sources.list.d/buster-backports.list`
like above except the content is pointing to buster-backports.

[source,bash]
----
# /etc/apt/sources.list.d/buster-backports.list
deb http://deb.debian.org/debian buster-backports main contrib non-free
----

Once you've added one of these files you need to update the database of the
package managing system so your preferred package manager is knowing about
the new added source and the package versions from there.

[source,bash]
----
sudo apt update
----

For more information about backports you might want to visit the Debian Wiki
site about https://wiki.debian.org/Backports[Backports] and / or also the
https://backports.debian.org/Instructions/[Backports website].

=== Other KiCad related packages and their relations

For more flexibility the various KiCad packages are provided by also various
binary packages you might want need to install beside the `kicad` main package
that is containing the KiCad applications. For example there are existing
dedicated packages for the footprints, symbols, templates and 3D-models and
also KiCad documentation related packages which aren't necessarily
automatically get installed (depends on the configuration of your system) if
you install the `kicad` package.

If you are looking for all KiCad related packages you can search for them
simply by your preferred package manager tool, e.g. by `apt`.

[source,bash]
----
apt search kicad
----

For a useful working with KiCad you will need to have installed the following
packages: `kicad` and `kicad-libraries` and sometimes also the package
`kicad-demos`.
Typically these packages get installed automatically if you haven't changed the
system default on installing of recommended packages. Please don't install the
package `kicad-common` directly, it's a transitional package and will go away,
it can get removed if you've installed this.

You can get more information about the relation between the various `kicad`
packages in Debian please have a look at a graphic on the
https://wiki.debian.org/KiCad[Debian wiki page] for KiCad packages.

'''

=== KiCad packages within the Debian releases

==== Debian 9 Old-Old-Stable (Codename Stretch)

WARNING: The Stretch release isn't supported any more and wont get updates, please
upgrade your system to at least Debian Buster!

'''

==== Debian 10 Old-Stable (Codename Buster)

===== https://packages.debian.org/buster-backports/kicad[*Old-Stable* (Backports)]

Version: {{< repology debian_10_backports >}}

====== Installation

[source.bash]
----
sudo apt install -t buster-backports kicad
----

Please ensure you have updated the local database of the package management
system before installing!

'''

===== https://packages.debian.org/buster/kicad[*Old-Stable* (Release)]

Version: {{< repology debian_10 >}}

====== Installation

[source.bash]
----
sudo apt install kicad
----

''''

==== Debian 11 Stable (Codename Bullseye)

===== https://packages.debian.org/bullseye-backports/kicad[Stable (Backports)]

Version: {{< repology debian_11_backports >}}

====== Installation

Please ensure you have updated the local database of the package management
system before proceed.

[source.bash]
----
sudo apt install -t bullseye-backports kicad
----


===== https://packages.debian.org/bullseye/kicad[Stable]

Version: {{< repology debian_11 >}}

'''

====== Installation

[source.bash]
----
sudo apt install kicad
----

'''

==== Debian Testing (Codename Bookworm)

===== https://packages.debian.org/bookworm/kicad[Stable (Release)]

Version: {{< repology debian_12 >}}

===== Installation

[source.bash]
----
sudo apt install kicad
----

'''

==== Debian Unstable (Sid)

Version: {{< repology debian_unstable >}}

===== Installation

[source.bash]
sudo apt install kicad

'''

=== Build KiCad from Source
You can find the instructions to build from source
link:https://dev-docs.kicad.org/en/build/[here].
If you use Debian stable with actual packages from Backports or you working
with the testing/sid release you can compile your own version of KiCad. Since
summer 2018 the required ngspice library libngspice for schematic simulation is
also available in testing/sid and also by the backports repositories.

Ensure you have installed some build dependencies at least before you try to
start own builds:

[source.bash]
----
sudo apt install cmake doxygen libboost-context-dev libboost-dev \
libboost-system-dev libboost-test-dev libcairo2-dev libcurl4-openssl-dev \
libgl1-mesa-dev libglew-dev libglm-dev libngspice-dev liboce-foundation-dev \
liboce-ocaf-dev libssl-dev libwxbase3.0-dev libwxgtk3.0-dev python-dev \
python-wxgtk3.0-dev swig wx-common
----
