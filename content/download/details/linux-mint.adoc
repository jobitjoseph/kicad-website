+++
title = "Install on Linux Mint"
summary = "Install instructions for KiCad on Linx Mint"
+++
:dist: Linux Mint

include::./content/download/_supported-note.adoc[]

== Linux Mint Debian Incompatibility
Linux Mint Debian does not work with the PPA listed below.  Only Linux Mint Ubuntu and its derivatives have been
reported to work with this PPA.

== {{< param "release" >}} Stable Release

=== Installation
KiCad {{< param "release" >}} is available in https://launchpad.net/~kicad/+archive/ubuntu/kicad-8.0-releases[PPA for KiCad: 8.0 releases ].
include::./content/download/_kicad_ppa.adoc[]


== 7.0 Old Stable Release

KiCad 7.0 is available in https://launchpad.net/~kicad/+archive/ubuntu/kicad-7.0-releases[PPA for KiCad: 7.0 releases].
