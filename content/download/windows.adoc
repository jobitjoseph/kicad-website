+++
title = "Windows Downloads"
distro = "Windows"
summary = "Downloads KiCad for Windows 8.1, Windows 10 and Windows 11"
iconhtml = "<div><i class='fab fa-windows'></i></div>"
weight = 1
+++

[.initial-text]
KiCad supports Windows 10 and 11.  See
link:/help/system-requirements/[System Requirements] for more details.

[.initial-text]
== Stable Release

Current Version: *{{< param "release" >}}*

KiCad is available for Windows on the x86-64-bit and arm64 platforms.

++++
<div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="accordion-item">
		<div class="accordion-header" role="tab" id="mirrors-64bit-heading">
			<button role="button" class="accordion-button" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-64bit" aria-expanded="true" aria-controls="mirrors-64bit">
					64-bit x64
			</button>
		</div>
		<div id="mirrors-64bit" class="accordion-collapse collapse show" role="tabpanel" aria-labelledby="mirrors-64bit-heading">
			<div class="accordion-body">
				<h4>Worldwide</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://github.com/KiCad/kicad-source-mirror/releases/download/{{< param "release" >}}/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/github.svg" height="31" /> GitHub
					</a>
				</div>
				<h4>Europe</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
				</div>
				<h4>Asia</h4>
				<div class="list-group download-list-group">

					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" />AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/chongqing.jpeg" />Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>North America</h4>
					<a class="list-group-item dl-link" href="https://mirror.clarkson.edu/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/clarkson.png" />Clarkson University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Australia</h4>
					<a class="list-group-item dl-link" href="https://mirror.aarnet.edu.au/pub/kicad/windows/stable/kicad-{{< param "release" >}}-x86_64.exe">
						<img src="/img/download/aarnet_logo_white.svg" height="31" > AARNet
					</a>
				</div>
			</div>
		</div>
	</div><div class="accordion-item">
	<div class="accordion-header" role="tab" id="mirrors-arm64-heading">
			<button role="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-parent="#accordion" href="#mirrors-arm64" aria-expanded="true" aria-controls="mirrors-arm64">
					arm64
			</button>
		</div>
		<div id="mirrors-arm64" class="accordion-collapse collapse show" role="tabpanel" aria-labelledby="mirrors-arm64-heading">
			<div class="accordion-body">
				<h4>Worldwide</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://github.com/KiCad/kicad-source-mirror/releases/download/{{< param "release" >}}/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/download/github.svg" height="31" /> GitHub
					</a>
				</div>
				<h4>Europe</h4>
				<div class="list-group download-list-group">
					<a class="list-group-item dl-link" href="https://kicad-downloads.s3.cern.ch/windows/stable/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/about/cern-logo.png" /> CERN - Switzerland
					</a>
				</div>
				<h4>Asia</h4>
				<div class="list-group download-list-group">

					<a class="list-group-item dl-link" href="https://mirrors.aliyun.com/kicad/windows/stable/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/download/aliyun.png" border="0" alt="AliCloud" />AlibabaCloud
					</a>
					<a class="list-group-item dl-link" href="https://mirrors.cqu.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/download/chongqing.jpeg" />Chongqing University
					</a>
					<a class="list-group-item dl-link" href="https://mirror.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/download/tuna.png" />Tsinghua University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>North America</h4>
					<a class="list-group-item dl-link" href="https://mirror.clarkson.edu/kicad/windows/stable/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/download/clarkson.png" />Clarkson University
					</a>
				</div>
				<div class="list-group download-list-group">
					<h4>Australia</h4>
					<a class="list-group-item dl-link" href="https://mirror.aarnet.edu.au/pub/kicad/windows/stable/kicad-{{< param "release" >}}-arm64.exe">
						<img src="/img/download/aarnet_logo_white.svg" height="31" > AARNet
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
++++

[.donate-hidden]
== {nbsp}
++++
	{{< getpartial "download_thanks.html" >}}
++++



== Download Verification
All installer binaries will have a code signed digital signature attached. Windows will
automatically verify the signature is valid, however you may want to ensure it actually is
present and the correct signer. A guide on how to verify the installer is available here:
link:/help/windows-download-verification/[Windows Installer Verification Guide]

A valid official KiCad signature has one of these certificate infos:

[role="table table-striped table-condensed"]
|===
|Signer Name|*KICAD SERVICES CORPORATION*
|Issuer|*GlobalSign GCC R45 EV CodeSigning CA 2020*
|Serial Number|*39df6b588d969ca15d8d2756*
|Validity Range| 2024-12-20 to 2028-03-06
|===

[role="table table-striped table-condensed"]
|===
|Signer Name|*KICAD SERVICES CORPORATION*
|Issuer|*GlobalSign GCC R45 EV CodeSigning CA 2020*
|Serial Number|*3e1be0c88cf5ab57cbfcae27*
|Validity Range| 2022-02-04 to 2025-02-04
|===

[role="table table-striped table-condensed"]
|===
|Signer Name|*KiCad Services Corporation*
|Issuer|*Sectigo RSA Code Signing CA*
|Serial Number|*1f70b098b5c21a254a6fb427cdf8893e*
|===


== Previous Releases

Previous releases should be available for download on:

https://downloads.kicad.org/kicad/windows/explore/stable


== Testing Builds

The _testing_ builds are snapshots of the current stable release codebase at a specific time.
These contain the most recent changes that will be included in the next bugfix release in the
current stable series.  For example, if the current stable release is 8.0.0, these builds will
contain changes destined for version 8.0.1.

https://downloads.kicad.org/kicad/windows/explore/8.0-testing


== Nightly Development Builds

The _nightly development_ builds are snapshots of the development (master branch) codebase at a
specific time.  This codebase is under active development, and while we try our best, may contain
more bugs than usual.  New features added to KiCad can be tested in these builds.

WARNING: Please read link:/help/nightlies-and-rcs/[Nightly Builds and Release Candidates] for
		 important information about the risks and drawbacks of using nightly builds.

https://downloads.kicad.org/kicad/windows/explore/nightlies
