+++
title = "Vimdrones AM32 ESC development board"
projectdeveloper = "Huibean Luo"
projecturl = "https://github.com/VimDrones/AM32_esc_development_board"
"made-with-kicad/categories" = [
    "Motor Controller",
    "Development Board",
    "Drones",
    "DroneCAN"
]
+++

link:https://dev.vimdrones.com/products/vimdrones_esc_dev/[Vimdrones AM32 ESC Development Board] is designed for the link:https://github.com/am32-firmware/AM32[AM32] open-source ESC project, featuring a built-in motor and CAN port for seamless development and testing with DroneCAN