+++
title = "Glasgow revC"
projectdeveloper = "Catherine 'whitequark'"
projecturl = "https://github.com/GlasgowEmbedded/glasgow"
"made-with-kicad/categories" = [
    "Development Board"
]
+++

Glasgow is a tool for exploring digital interfaces, aimed at embedded
developers, reverse engineers, digital archivists, electronics hobbyists,
and everyone else who wants to communicate to a wide selection of digital
devices with high reliability and minimum hassle.
